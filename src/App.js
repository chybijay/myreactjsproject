import React, { Component } from 'react'
import HomePage from './pages/HomePage'
import AboutPage from './pages/AboutPage'
import ContactPage from './pages/ContactPage'
import Footer from './components/Footer'
import Navbar from './components/Navbar'
import {Switch,Route,BrowserRouter} from 'react-router-dom'
import './App.css'


export default class App extends Component {
  render() {
    return (
      <>
      <Navbar/>
      <Footer />
      <BrowserRouter>
      <Switch>
      < Route exact path="/" component={HomePage}/>
        < Route exact path="/AboutPage" component={AboutPage}/>
        < Route exact path="/ContactPage" component={ContactPage}/>
      </Switch>
      </BrowserRouter>

      </>
    )
  }
}
