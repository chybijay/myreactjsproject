import React from 'react';



function Footer() {

    return(
        <footer>
        <nav class="navbar fixed-bottom navbar-light bg-light">
        <a class="navbar-brand" href="/"></a>
        <p>Ⓒ Copyright 2020 MB Technology. All Right Reserved.</p>
    
        </nav>
      
        </footer>
    );

}

export default Footer;